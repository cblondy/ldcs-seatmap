/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DeckDataService } from './deck-data.service';

describe('Service: DeckData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeckDataService]
    });
  });

  it('should ...', inject([DeckDataService], (service: DeckDataService) => {
    expect(service).toBeTruthy();
  }));
});
