import { Injectable } from '@angular/core';
import { Deck } from '../models/Deck';

@Injectable({
  providedIn: 'root'
})
export class DeckDataService {

  deckList : Map<string,Deck>;

constructor() { }

}
