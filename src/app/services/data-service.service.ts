import { Injectable } from '@angular/core';
import { Zone } from '../models/zone';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
  dialogData: any;


  private routerInfo: BehaviorSubject<Zone[]>;
  zoneList : Zone[] = [];

constructor() {
  this.routerInfo = new BehaviorSubject<Zone[]>([]);
}

getDialogData() {
  return this.dialogData;
}
// DEMO ONLY, you can find working methods below
addItem (item: any): void {
  this.dialogData = item;
}

updateItem (item: any): void {
  this.dialogData = item;
}

deleteItem (item: any): void {
  //console.log(id);
}

setValue(newValue): void {
  this.routerInfo.next(newValue);
}

getValue(): Observable<Zone[]> {
  return this.routerInfo.asObservable();
}

}
