export interface DialogData {
  title: string;
  label: string;
  message:string;
  value: any;

  //for selection list
  referenceTableName: string;
  id_field: string;
  main_field: string;
  additional_fields: string[];
  //items: any[];
}
