import { Zone } from './zone';

export class Deck {

  id : number;
  zoneList : Zone[];
  deckName : string;
}
