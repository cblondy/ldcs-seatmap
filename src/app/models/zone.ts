import { RowSeat } from './row-seat';
import { Seat } from './seat';

export class Zone {
  zoneLabel: string;
  //rowsSeatsList : RowSeat[] ;
  rowsSeatsList: RowSeat[]  = [];
  rowsSeatsListRight: RowSeat[]  = [];
  rowsSeatsListCenter: RowSeat[]  = [];
  rowsSeatsListLeft: RowSeat[]  = [];
  rowLength: number;

  seatsClass: string;

  rowStart: number;
  rowEnd: number;

  excludedSeats: number[] = [];

  seatList ?: Seat[] = [];



}
