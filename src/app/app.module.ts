import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SeatingMapComponent } from './seating-map/seating-map.component';

import { DfApiModule, Configuration, LoginModule } from 'eaf-lib';
import { environment } from 'src/environments/environment';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MatTabsModule, MatTableModule } from '@angular/material';
import { SeatingMapModule } from './seating-map/seating-map.module';
import { ZoneFormComponent } from './seating-map/zone-form/zone-form.component';
import { MaterialFeaturesModule } from './material-features/material-features.module';
import { InputDialogComponent } from './input-dialog/input-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

export function dfApiConfigurationFactory(){
  let c = new Configuration(environment.dfApi);
  console.log('[dfApiConfigurationFactory]', c, environment);
  return c;
}

@NgModule({
   declarations: [
      AppComponent,
      InputDialogComponent
   ],
   imports: [
      BrowserModule,
      SeatingMapModule,
      DragDropModule ,
      LoginModule,
      HttpClientModule,
      AppRoutingModule,
      MaterialFeaturesModule,
      DfApiModule.forRoot(dfApiConfigurationFactory),
      BrowserAnimationsModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
