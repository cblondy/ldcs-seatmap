import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeatingMapComponent } from './seating-map.component';
import { MatTabsModule, MatGridListModule, MatTableModule } from '@angular/material';
import { DeckMapComponent } from './deck-map/deck-map.component';
import { MaterialFeaturesModule } from '../material-features/material-features.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AppModule } from '../app.module';
import { InputDialogComponent } from '../input-dialog/input-dialog.component';
import { ZoneFormComponent } from './zone-form/zone-form.component';
import { DeckFormComponent } from './deck-form/deck-form.component';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    MaterialFeaturesModule,
    DragDropModule
  ],
  declarations: [
    SeatingMapComponent,
    DeckMapComponent,
    ZoneFormComponent,
    DeckFormComponent
  ],
  exports: [
    SeatingMapComponent
  ],
  entryComponents: [
    ZoneFormComponent,
    DeckFormComponent
  ]
})
export class SeatingMapModule { }
