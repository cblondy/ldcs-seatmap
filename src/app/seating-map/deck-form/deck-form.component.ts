import { Component, OnInit, Inject } from '@angular/core';
import { Zone } from 'src/app/models/zone';
import { MatDialogRef, MAT_DIALOG_DATA, MatChipInputEvent, MatSnackBar } from '@angular/material';
import { InputDialogComponent } from 'src/app/input-dialog/input-dialog.component';
import { DataServiceService } from 'src/app/services/data-service.service';
import { ZoneFormData } from '../zone-form/zone-form-data';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { DeckFormDeck } from './deck-form-deck';
import { RowSeat } from 'src/app/models/row-seat';
import { DeckMapComponent } from '../deck-map/deck-map.component';

@Component({
  selector: 'app-deck-form',
  templateUrl: './deck-form.component.html',
  styleUrls: ['./deck-form.component.css']
})
export class DeckFormComponent implements OnInit {

  zoneList : Zone[] = [];



  isValidRanges : Map<Zone,boolean> = new Map;
  isValidLabels : Map<Zone,boolean> = new Map;
  isValidRowsDefinition : Map<Zone,boolean> = new Map;

  deckName : string;
  visible = true;
  isFocusOff = false;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(
    public dataService: DataServiceService, private snackBar: MatSnackBar,
    ) {
      //this.deckName = formData.deckName;
  }

  ngOnInit() {
  }


  focusOutFunction() : void{
    this.isFocusOff = true;
  }

  onValidate() {

    console.log("deck-form.component::onValidate");
    let message : string;
    let cancel : boolean = false;

    if(this.isValidLabels.size<1){
      cancel = true;
      message = 'Invalid name.';
    }
    this.isValidLabels.forEach( (value : boolean, key : Zone , map) => {
      console.log("Labels ite ",value);
      if(!value){
        cancel = true;
        message = 'Invalid name.';
      }

    });

    console.log(this.isValidRowsDefinition);
    if(this.isValidRowsDefinition.size<1){
      cancel = true;
      message = 'Invalid rows definition.';
    }
    this.isValidRowsDefinition.forEach( (value, key, map) => {
      console.log("Rows Def. ite",value);
      if(!value){
        cancel = true;
        message = 'Invalid rows definition.';
      }

    });

    if(this.isValidRanges.size<1){
      cancel = true;
      message = 'Invalid range.';
    }
    this.isValidRanges.forEach( (value, key, map) => {

      console.log("Range ite",value);
      if(!value){
        cancel = true;
        message = 'Invalid range.';
      }

    });

    if(!cancel){
      console.log(cancel);
      let zoneBis : Zone [] = Object.assign({}, this.zoneList);
      this.dataService.setValue(this.zoneList);
    }else {

      this.snackBar.open(message, '', {
        duration: 2000,
        panelClass: ['error_snackbar']
      });
    }
    //this.dialogRef.close(this.formRecord);

  }

  deleteZone(zone: Zone): void {
    const index = this.zoneList.indexOf(zone);

    if (index >= 0) {
      this.zoneList.splice(index, 1);
      //this.rowList.splice(index,1);
    }
  }

 /* addRow(event: MatChipInputEvent, excludedList: number[]): void {
    const input = event.input;
    const value = event.value;

    console.log(event);
    // Add our fruit
    if (!isNaN(+value) && value.trim().length > 0 && !excludedList.includes(+value)) {
      excludedList.push(+value);
      //this.rowList.push(value.trim());
    } else if (value.length > 0) {
      this.isFocusOff = false;
      this.snackBar.open('Entry already existing or not numerical.', '', {
        duration: 2000,
        panelClass: ['warning_snackbar']
      });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

  }*/

  rowListChanged(zone : Zone,rows : string,rowSeatList : RowSeat[] ,position : string){

    let rowsTab : string[] = rows.split(',');

    rowsTab.forEach(element => {
      let rowSeat : RowSeat = new RowSeat;

      rowSeat.name = element.trim();
      rowSeat.position = position;

      if (rowSeatList.indexOf(rowSeat)== -1){
        rowSeatList.push(rowSeat);
      }

    });

    this.definitionChanged(zone);

    console.log(rowSeatList);


  }

  rangeChanged(zone : Zone): void{
    console.log("RANGE CHANGED",zone.rowEnd,zone.rowStart);
    if (zone.rowEnd < zone.rowStart){
      this.snackBar.open('Invalid range', '', {
        duration: 2000,
        panelClass: ['warning_snackbar']
      });
      this.isValidRanges.set(zone, false);
    } else {
      this.isValidRanges.set(zone, true);
      zone.rowLength = ( zone.rowEnd - zone.rowStart ) - zone.excludedSeats.length;
    }

    console.log(this.isValidRanges);
  }

  nameChanged(zone : Zone): void {
    console.log("NAME CHANGED",zone.rowEnd,zone.rowStart);
    if (zone.zoneLabel.length < 1){
      this.snackBar.open('Invalid name', '', {
        duration: 2000,
        panelClass: ['warning_snackbar']
      });
      this.isValidLabels.set(zone, false);
    } else {
      this.isValidLabels.set(zone, true);
    }

    console.log(this.isValidLabels);
  }

  definitionChanged(zone : Zone): void {
    console.log("DEFINITION CHANGED",zone.rowEnd,zone.rowStart);

    zone.rowsSeatsList = zone.rowsSeatsList.concat(zone.rowsSeatsListCenter,zone.rowsSeatsListLeft,zone.rowsSeatsListRight);
    if (zone.rowsSeatsList.length < 1 ){

      this.isValidRowsDefinition.set(zone, false);
    } else {
      this.isValidRowsDefinition.set(zone, true);
    }
    console.log(this.isValidLabels);
  }

  removeRow(excluded: number, excludedList : number[], isRowDefinition : boolean, zone : Zone): void {
    const index = excludedList.indexOf(excluded);
    if (index >= 0) {
      excludedList.splice(index, 1);
    }

    if(isRowDefinition){
      this.definitionChanged(zone);
    }

  }

  createZone(){
    let message : string;
    let cancel : boolean = false;

    if (!cancel) {
      let zone : Zone = new Zone();
      this.zoneList.push(zone);

      this.isValidRanges.set(zone, false);
      this.isValidLabels.set(zone, false);
      this.isValidRowsDefinition.set(zone, false);
    } else {
      this.snackBar.open(message, '', {
        duration: 2000,
        panelClass: ['error_snackbar']
      });
    }
  }
  onCancel() {
   // this.dialogRef.close(0);
   }

   duplicateZone(zone : Zone) {
    this.zoneList.push(Object.assign({}, zone));
    }
}
