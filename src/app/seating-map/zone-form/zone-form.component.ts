import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatChipInputEvent } from '@angular/material';
import { ZoneFormData } from './zone-form-data';
import { DataServiceService } from '../../services/data-service.service';
import { InputDialogComponent } from '../../input-dialog/input-dialog.component';
import { Zone } from 'src/app/models/zone';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { RowSeat } from 'src/app/models/row-seat';

@Component({
  selector: 'app-zone-form',
  templateUrl: './zone-form.component.html',
  styleUrls: ['./zone-form.component.css']
})
export class ZoneFormComponent implements OnInit {


  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  rowsSeat: RowSeat[] = [];

  zoneLabel : string;
  rowList : string[] = [];
  rowLength : number;

  constructor(public dialogRef: MatDialogRef<InputDialogComponent>,
    public dataService: DataServiceService,
    @Inject(MAT_DIALOG_DATA) public formData: ZoneFormData) {
      if(formData.zone != undefined){
      this.zoneLabel = formData.zone.zoneLabel;
      this.rowLength = formData.zone.rowLength;
     // this.rowsSeat = formData.zone.rowsSeatsList;
    }
    }

  ngOnInit() {
  }

  onCancel() {
   this.dialogRef.close(0);
  }

  onValidate() {

    let zone : Zone = new Zone();
    zone.zoneLabel = this.zoneLabel;
    //zone.rowsSeatsList = this.rowsSeat;
    zone.rowLength = this.rowLength;

    this.dialogRef.close(zone);
    //this.dialogRef.close(this.formRecord);

  }

  addRow(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.rowsSeat.push({name: value.trim()});
      //this.rowList.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeRow(rowSeat: RowSeat): void {
    const index = this.rowsSeat.indexOf(rowSeat);

    if (index >= 0) {
      this.rowsSeat.splice(index, 1);
      //this.rowList.splice(index,1);
    }
  }

  saverange(i : number){
    console.log(i);
  }
}
