import { RowSeat } from 'src/app/models/row-seat';
import { Zone } from 'src/app/models/zone';

export class ZoneFormData {
  zone?: Zone;
}
