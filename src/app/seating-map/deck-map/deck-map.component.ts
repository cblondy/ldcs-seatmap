import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Seat } from 'src/app/models/seat';
import { DataServiceService } from 'src/app/services/data-service.service';
import { ZoneFormComponent } from '../zone-form/zone-form.component';
import { Zone } from 'src/app/models/zone';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { DeckDataService } from 'src/app/services/deck-data.service';
import { Deck } from 'src/app/models/Deck';
import { DeckFormComponent } from '../deck-form/deck-form.component';

@Component({
  selector: 'app-deck-map',
  templateUrl: './deck-map.component.html',
  styleUrls: ['./deck-map.component.css']
})
export class DeckMapComponent implements OnInit {

  set zoneList(v: Zone[]) {
		console.log("Generate PIE");
    this._zoneList = v;

    console.log(this.zoneList);
		this.zoneList.forEach(zone =>{


      this.dataSource = new MatTableDataSource<Seat>();

      zone.rowsSeatsList.forEach(element => {

        let i = 0;
        this.dataSource = new MatTableDataSource<Seat>();

        while(i<zone.rowLength) {
          let seat : Seat = this.addNewSeat(zone, element.name);
          zone.seatList.push(seat);
          this.dataSource.data.push(seat);
          i++;
        }

        this.displayedTableDataSources.set(zone.zoneLabel+element.name, this.dataSource);

        console.log('TEST',  this.displayedTableDataSources.get(zone.zoneLabel+element.name));
      });


    });
	}
	get zoneList(): Zone[] {
		return this._zoneList;
	}


  constructor(public deckService: DeckDataService, public dataService: DataServiceService, public dialog: MatDialog) {

    this.dataService.getValue().subscribe(list => {

        this.zoneList = list;

  });

  }
  @Input() deckName: string;

  displayedTables: string[];
  displayedTableDataSources: Map<string, MatTableDataSource<Seat>> = new Map;
  displayedTableColumns: Map<string, string[]>;


  _zoneList: Zone[]  ;


  idSeats: string;

  idSeatsCount = 0;

  displayedColumns: string[] = ['position', 'name', 'separator', 'weight', 'symbol'];
  dataSource: MatTableDataSource<Seat> = new MatTableDataSource<Seat>();

  signlist: string[] = [];
  offset: any;
  initialPosition = { x: 100, y: 100 };
  position = { x: 0, y: 0 };

  ngOnInit() {
  }

  createExitSign(id: string) {
    this.signlist.push(id + this.signlist.length.toString);
  }

  deleteZone(zone: Zone) {
    this.zoneList.splice(this.zoneList.indexOf(zone), 1);
    let i = 0;
    let key: string;
    const iterator = this.displayedTableDataSources.keys();
    while (i < this.displayedTableDataSources.size) {
      key = iterator.next().value;

      if (key.indexOf(zone.zoneLabel)  !== -1 ) {
        this.displayedTableDataSources.delete(key);
      }

      i++;
    }
  }

  editZone(zone: Zone) {

    const formItem = Object.assign({}, zone);

    const formDialog = this.dialog.open(DeckFormComponent, {
      minWidth: '600px',
      maxHeight: '120vh',
      data: {
        deckName : this.deckName
      }
    });


    formDialog.afterClosed().subscribe(result => {
      if (result != 0 && result != undefined) {

        const editedZone: Zone = result;
        console.log(result);
        this.zoneList.push(result);
        this.dataSource = new MatTableDataSource<Seat>();

        zone.rowsSeatsList.forEach(element => {

          let i = 0;
          this.dataSource = new MatTableDataSource<Seat>();

          while (i < zone.rowLength) {
            this.dataSource.data.push(this.addNewSeat(zone, element.name));
            i++;
          }

          this.displayedTableDataSources.set(zone.zoneLabel + element.name, this.dataSource);
          console.log('TEST',  this.displayedTableDataSources.get(zone.zoneLabel + element.name));
        });

      }
    });
  }

  saveDeck(deckLvl: string) {
    let deck: Deck;
    deck.zoneList = this.zoneList;
    this.deckService.deckList.set(deckLvl, deck);

    console.log(this.deckService.deckList.set('Main Deck', deck));
  }

  public createZone() {
    const formDialog = this.dialog.open(DeckFormComponent, {
      minWidth: '600px',
      maxHeight: '120vh',
      data: {
        deckName : this.deckName
      }
    });


    formDialog.afterClosed().subscribe(result => {
      if (result != 0 && result != undefined) {

        this.zoneList = result;

        this.zoneList.forEach(zone =>{


          this.dataSource = new MatTableDataSource<Seat>();

          zone.rowsSeatsList.forEach(element => {

            let i = 0;
            this.dataSource = new MatTableDataSource<Seat>();

            while(i<zone.rowLength) {
              let seat : Seat = this.addNewSeat(zone, element.name);
              zone.seatList.push(seat);
              this.dataSource.data.push(seat);
              i++;
            }

            this.displayedTableDataSources.set(zone.zoneLabel+element.name, this.dataSource);

            console.log('TEST',  this.displayedTableDataSources.get(zone.zoneLabel+element.name));
          });


        });
      /*  let zone : Zone = result;
        console.log(result);
        this.zoneList.push(zone);
        this.dataSource = new MatTableDataSource<Seat>();

        zone.rowsSeatsList.forEach(element => {

          let i = 0;
          this.dataSource = new MatTableDataSource<Seat>();

          while(i<zone.rowLength){
            let seat : Seat = this.addNewSeat(zone.zoneLabel,element.name);
            zone.seatList.push(seat)
            this.dataSource.data.push(seat);
            i++;
          }

          this.displayedTableDataSources.set(zone.zoneLabel+element.name, this.dataSource);

          console.log('TEST',  this.displayedTableDataSources.get(zone.zoneLabel+element.name));
        });*/

      }
    });

  }

  seatChangeStatus(element : Seat) : void{
    console.log(element);
    element.status = !element.status
  }

  addNewSeat(zone: Zone, column: string): Seat {
    const seat = new Seat;
    const id = zone.zoneLabel + column;

    console.log("ZONE",zone);

    let done = false;
    if (this.idSeats != id) {
      this.idSeats = id;
      this.idSeatsCount = zone.rowStart;
    }

    seat.status = false;

    while(!done){
      if(!zone.excludedSeats.includes(this.idSeatsCount)) {
        seat.text = column + this.idSeatsCount.toString();
        done = true;
      }


      this.idSeatsCount++;
    }

    return seat;
  }

  getDataSource(id: string):  MatTableDataSource<Seat> {

    return this.displayedTableDataSources.get(id);

  }

  getColumnWidth(id: string): number {
    console.log(this.displayedTableDataSources.get(id).data.length,' ', id);
    console.log((100 - 20) / (this.displayedTableDataSources.get(id).data.length));
    return (100 - 20) / (this.displayedTableDataSources.size);
  }
  /*dragEnd(event: CdkDragEnd) {
    this.offset = { ...(<any>event.source._dragRef)._passiveTransform };

    this.position.x = this.initialPosition.x + this.offset.x;
    this.position.y = this.initialPosition.y + this.offset.y;

    console.log(this.position, this.initialPosition, this.offset);
  }*/

  dragEnd(event: CdkDragEnd) {
    console.log(event);
    this.offset = { ...(<any>event.source._dragRef)._passiveTransform };

    console.log(this.offset);

    this.position.x = this.initialPosition.x + this.offset.x;
    this.position.y = this.initialPosition.y + this.offset.y;

    console.log(this.position, this.initialPosition, this.offset);
  }


  /*dragEnded($event: CdkDragEnd) {
    const { offsetLeft, offsetTop } = $event.source.element.nativeElement;
    const { x, y } = $event.distance;
    this.positionX = offsetLeft + x;
    this.positionY = offsetTop + y;
    this.showPopup = true;
    console.log({ positionX, positionY });
  }*/

}
