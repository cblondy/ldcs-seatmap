import {Directive, ChangeDetectorRef, ContentChild, AfterViewInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material';

export abstract class CustomMatFormFieldControl<T> extends MatFormFieldControl<T> {
  required: boolean;
}

/**
 * Patch mat-form-field for 'required' attribut in Reactive Form
 * @see https://github.com/angular/material2/issues/2574#issuecomment-437323470
 */
@Directive({
// tslint:disable-next-line: directive-selector
  selector: 'mat-form-field',
})
export class MatFormFieldRequiredDirective implements AfterViewInit {
  @ContentChild(MatFormFieldControl) _control: CustomMatFormFieldControl<any>;

  constructor(private _cdRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    if (this._control && this._control.ngControl) {

      const validator =
        this._control.ngControl.validator && this._control.ngControl['form'] // [formControl] standalone
          ? this._control.ngControl['form'].validator
          : this._control.ngControl.control // formControlName in FromGroup
          ? this._control.ngControl.control.validator
          : null;
      if (validator) {
        Promise.resolve().then(() => {
          const newValidator = validator(new FormControl());
          if (!!newValidator) {
            this._control.required = newValidator.required;
            this._cdRef.markForCheck();
          }
        });
      }
    }
  }

}
