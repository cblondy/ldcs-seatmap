#!/bin/sh

function bk_auth_basic() {
	if [ x${BB_AUTH_STRING} != x ] ; then
		return;
	fi
	echo "Bitbucket Account:";
	# local l_username=`git config user.name`;
	# local l_username=`git config remote.origin.url | grep -Po "/(\w+)@"`;
	# l_username=${l_username:1:-1};
	# read -p "Username : [$l_username]" username
	# if [ -z $username ] ; then
		# username=$l_username;
	# fi

	local l_username=`git config user.email`;
	read -p "Email : [$l_username]" username
	if [ -z $l_username ] ; then
		username=$l_username;
	fi

	read -s -p "Password :" password
	echo;

	BB_AUTH_STRING="$username:$password";
}

function dl_internale_lib() {
	# branch=$1;
	# version=$2;
	
	local project=$1;
	# local branch=; <empty> | master | develop
	local branch=$2;
	# local version= lastest | spec x.x.x
	local version=$3;

	echo;
	echo "DOWNLOAD $project $branch $version version From Bitbucket";

	local url="https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads/$project-$branch-$version.tgz";

	local lib_path="./3rdParty/$project";
	
	curl -L -o "$lib_path.tgz" --user ${BB_AUTH_STRING} "$url"
	
	local filesize=`stat -c %s $lib_path.tgz`;

	echo "url:     $url"       > "$lib_path.txt";
	echo "branch:  $branch"   >> "$lib_path.txt";
	echo "version: $version"  >> "$lib_path.txt";
	echo "size:    $filesize" >> "$lib_path.txt";


	if [ "$filesize" -ge "10240" ] ; then
		echo -e "\e[32m Success";
	else
		echo -e "\e[1;31m File too small. Maybe Failed";
	fi
	echo -e "\e[0m";
}

bk_auth_basic

BITBUCKET_REPO_OWNER=AM-Solutions;
BITBUCKET_REPO_SLUG=eaflib-angular;

dl_internale_lib $1 $2 $3
