#!/bin/sh

function df_auth_basic() {
	echo "Dreamfactory Account";
	read -p "Username : " email
	email="$email@edge-airport.com";

	read -s -p "Password : [Rport606]" password
	if [ -z $password ] ; then
		password=Rport606;
	fi

	basic_auth=`echo -n "$email:$password" | base64`;
	echo;
}

function http_code_glossary() {
	echo;
	echo "HTTP CODE :  Reason";
	echo "200 & >   :  OK";
	echo "201       :  Created";
	echo "400 & >   :  Error Client-Side";
	echo "401       :  Not Authenticated";
	echo "403       :  Forbidden";
	echo "404       :  Not Found";
	echo "405       :  Not Allowed";
	echo "408       :  Time-out";
	echo "500 & >   :  Error Server-Side";
	echo "501       :  Not Implement";
	echo "503       :  Service Unavailable";
	echo "504	    :  Gateway Time-out";
}

function display_http_code() {
	local code=$1;

	if [ "$code" -ge "400" ] ; then
		echo -n -e "\e[1;31m $code ";
	else
		echo -n -e "\e[32m $code ";
	fi
	echo -e `http_code_glossary | grep "^$code" | cut -c8-` "\e[0m";
}

function df_deploy_app() {
	local host=$1;
	local app_name=$2;

	local storage=files/applications;

	echo;
	echo "DEPLOY $app_name to $host";

	echo -n "DELETE OLD APP: ";
	local result=`curl --connect-timeout 5 -s -i -k -3 -X DELETE --header "Authorization: Basic $basic_auth" "http://$host/api/v2/$storage/$app_name/?force=true"`;
	local http_code=`echo "$result" | grep '^HTTP/' | tail -1 | cut -d ' ' -f 2`;
	display_http_code $http_code;


	echo -n "POST & Extract NEW APP: ";
	local result=`curl -s -i -k -3 -X POST --header "Content-Type: multipart/form-data" --form "files=@$app_name.zip;type=application/x-zip-compressed" --header "Authorization: Basic $basic_auth" "http://$host/api/v2/$storage/?extract=true"`;
	local http_code=`echo "$result" | grep '^HTTP/' | tail -1 | cut -d ' ' -f 2`;
	display_http_code $http_code;
	echo;
	echo $result;
}

cd dist

df_auth_basic



df_deploy_app sam-api-server.bitnamiapp.com dcs-seat-map
